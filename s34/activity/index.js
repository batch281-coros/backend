const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

users = [

	{
		"username": "johndoe",
		"password": "johndoe1234"
	},
	{
		"username": "janedoe",
		"password": "johndoe1234"
	}
]

// GET home

app.get("/home", (req, res) => {
	res.send(`Welcome to the homepage`)
})

// GET users

app.get("/users", (req, res) => {
	res.send(users)
})

// DELETE user

app.delete("/delete-user", (req, res) => {

	const username = req.body.username

	let message;

	if(users.length > 0) {
		for(let i = 0; i<users.length; i++){
			if(username == users[i].username) {
				users.splice(i,1);
				message = `User ${username} has been deleted`;
				break;
			}
		}

		if(message == undefined){
			message = `User does not exist.`;
		}
	} else {
		message = `No users found.`;
	}

	res.send(message);
});

if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`));
}
module.exports = app;