const Course = require("../models/Course")

/*

module.exports.addCourse = (req) => {
  const userData = auth.decode(req.headers.authorization);

  if (!userData.isAdmin)
    return Promise.reject(new Error('You must be an admin'));

  const { name, description, price } = req.body;
  let newCourse = new Course({
    name,
    description,
    price
  });
  return newCourse.save().then((course, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

*/

module.exports.addCourse = (data) =>{

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	});

	return newCourse.save().then((course, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	});
};

// Retrieve all courses
module.exports.getAllCourses = (reqBody) => {
	return Course.find({}).then((result) => {
		return result;
	});
};

// Retrieve all active courses

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then((result) => {
		return result;
	});
};

// Retrieving a specifc course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then((result) => {
		return result;
	});
};

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		// Course not updated
		if(error){
			return false;

		// Course updated successfully
		} else {
			return true;
		};
	});
};

// Archive a course
module.exports.archiveCourse = (reqParams, data) => {

	let archiveCourse = {
		isActive: data.course.isActive
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	})
}