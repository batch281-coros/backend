const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();

// MongoDB
mongoose.connect("mongodb+srv://lemuelcoros:YmoAeS5qkSZyrpRd@wdc028-course-booking.tdobmlh.mongodb.net/E-CommerceAPI?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
);

let db = mongoose.connection

db.on("error", console.log.bind(console, "MongoDB connection error."));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})