const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// The find method returns a record if a match is found
		if(result.length > 0) {
			return true;
		// No duplicate emails found
		// The user is not registered in the database
		} else {
			return false;
		};
	});
};

// User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false;
		} else {
			return true;
		};
	});
};

// User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null){

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result)}
			} else {

				return false;
			};

		};
	});
};

// // Retrieve User Details
// module.exports.getProfile = (reqParams) => {

// 	return User.findById(reqParams.userId).then(result => {

// 		result.password = "";

// 		return result;
// 	});
// };

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

// User checkout
module.exports.checkout = async (data) => {

	let price;

	let productPrice = await Product.findById(data.productId).then(product => {

		price = product.price;

	})


	let isUserUpdated = await User.findById(data.userId).then( user => {

		user.orderedProduct.push({

			products: [{
				productId: data.productId,
				quantity: data.quantity
			}],
			totalAmount: (price * data.quantity).toFixed(2)
			
		});

		return user.save().then((user, error) => {

			if(error){
				return false;
			} else {
				return true;
			};
		});


	});


	let isProductUpdated = await Product.findById(data.productId).then( product => {

		product.userOrders.push({userId: data.userId})

		return product.save().then((product, error) => {

			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isProductUpdated){
		return true;
	} else {
		return false;
	}
};

// My Orders route

module.exports.myOrders = (data) => {

	return User.findById(data.userId).then(result => {


		return result.orderedProduct;
	});
};

// Set as admin
module.exports.setAsAdmin = (reqParams) => {

	let setAsAdmin = {
		isAdmin: true
	};

	return User.findByIdAndUpdate(reqParams.userId, setAsAdmin).then((result,error) => {

		if(error){
			return (`Error setting user as an admin.`)
		} else {
			return (`Successful setting user as an admin`)
		}
	})
}