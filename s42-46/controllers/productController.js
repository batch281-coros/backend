const Product = require("../models/Product");

// Add product
module.exports.addProduct = (data) => {

	let newProduct = new Product({

		name: data.product.name,
		description: data.product.description,
		price: data.product.price,
		category: data.product.category,
		image: data.product.image
	});

	return newProduct.save().then((product, error) => {

		if(error){
			return false
		} else {
			return true
		};
	});
};

// Get all prodcuts
module.exports.getAllProducts = () => {
	return Product.find({}).then((result) => {
		return result;
	});
};

// Retrieve all active products
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then((result) => {
		return result;
	});
};

// Retrieve a specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then((result) => {
		return result;
	});
};

// Update a product
module.exports.updateProduct = (reqParams, data) => {

	let updatedProduct = {
		name: data.product.name,
		description: data.product.description,
		price: data.product.price,
		category: data.product.category,
		image: data.product.image
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

		if(error){
			return false;
		} else {
			return true;
		};
	});
};

// Archive a product
module.exports.archiveProduct = (reqParams) => {

		let archiveProduct = {
		isActive: false
	};

		return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((course, error) => {

		if(error){
			return false;
		} else {
			return true;
		};
	});
};

// Activate a product
module.exports.activateProduct = (reqParams) => {

		let activateProduct = {
		isActive: true
	};

		return Product.findByIdAndUpdate(reqParams.productId, activateProduct).then((course, error) => {

		if(error){
			return false;
		} else {
			return true
		};
	});
};



