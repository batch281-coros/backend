const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
	},
	lastName: {
		type: String,

	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,

	},
	orderedProduct: [

		{
			products: [

				{
					productId: {
						type: String
					},
					productName: {
						type: String
					},
					quantity: {
						type: Number
					}
				}

			],

			totalAmount: {
				type: Number,
				default: 0
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}

		}
	]

})

module.exports = mongoose.model("User", userSchema);