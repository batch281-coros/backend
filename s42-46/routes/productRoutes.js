const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product
router.post("/createProduct", auth.verify, (req, res) => {

	const data = {

		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		productController.addProduct(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You must be an admin to create a product.`);
	};
});

// Route for retrieving all products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active products
router.get("/", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific course
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product
router.put("/update/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){

	productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You must be an admin to update a product.`)
	};
	
});

// Route for archiving a product
router.put("/:productId/archive", auth.verify, (req,res) => {

	const data = {
	
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You must be an admin to archive a product.`);
	};
});

// Route for activating a product
router.put("/:productId/activate", auth.verify, (req,res) => {

	const data = {
	
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You must be an admin to activate a product.`);
	};
});

module.exports = router;
