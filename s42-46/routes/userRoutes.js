const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the email already exists in the database
// Invokes the checkEmailExists function from the controller file
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// // Route for retrieving user
// router.get("/:userId/userDetails", auth.verify, (req, res) => {

// 	let data = {
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}
// 	if(data.isAdmin){
// 		userController.getProfile(req.params).then(resultFromController => res.send(resultFromController));
// 	} else {
// 		res.send(`You must be an admin`)
// 	}
	
// });

// Route for retrieving user details
// The "auth.verify" acts as a middleware to ensure that the user is logged in before they can enroll to a course
router.get("/details", auth.verify, (req, res) => {

	// Uses decode method defined in the auth.js file to retrieve user information from the token passing the token from the request header
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Route for user checkout
router.post("/checkout", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId,
		quantity: req.body.quantity
	};

	if(data.isAdmin === false ){
		userController.checkout(data).then(resultFromController => res.send(resultFromController));
	} 


	

});

// Route for getting authenticated user's order
router.get("/myOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.myOrders({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route for setting a user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {

	const userData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(userData.isAdmin == true){
		userController.setAsAdmin(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You must be an admin`)
	}

})

module.exports = router;