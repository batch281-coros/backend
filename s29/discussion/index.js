// [Section] Comparison Query Operators

// $gt/$gte

/*
	- allows us to find documents that have field numbers values greater than or equal to a specified field

	Syntax:
		db.collectionName.find({"field": { $gt: value }});
		db.collectionName.find({"field": { $gte: value }});
*/

db.users.find({ "age": { $gt: 50}});

db.users.find({ "age": { $gte: 21}});

// $lt/$lte

/*
	- allows us to find documents that have field numbers values less than or equal to a specified field

	Syntax:
		db.collectionName.find({"field": { $lt: value }});
		db.collectionName.find({"field": { $lte: value }});
*/

db.users.find({ "age": { $lt: 50}});

db.users.find({ "age": { $lte: 65}});

// $ne operator 

/*
	- allows us to find documents that have field numbers not equal to specified value

	Syntax:
		db.collectionName.find({"field": { $ne: value }});
		
*/

db.users.find({ "age": { $ne: 82}});

// $in operator

/*
	- allows us to find documents with specific match criteria one field using different values

	Syntax:
		db.collectionName.find({"field": { $in: value }});
*/

db.users.find({ "lastName": { $in: ["Hawking", "Doe"]}});

db.users.find({"course": { $in: ["HTML", "React"]}});

// Opposite of in is not in

db.users.find({"course": { $nin: ["HTML", "React"]}});

// [Section] Logical Query Operators

// $or operator

/*
	- allows us to find documents that match a single criteria from multiple provided search criteria

	Syntax:
		db.collectionName.find({ $or [{"fieldA": valueB}, {"fieldB": valueC}]);
*/

db.users.find({ $or: [{"firstName": "Neil"}, {"age": 25}]});

db.users.find({ $or: [{"firstName": "Neil"}, {"age": {$gt: 30}}]});

// $and operator

/*
	- allows us to find documents matching multiple criteria in a single field

	Syntax:
		db.collectionName.find({ $and [{"fieldA": valueB}, {"fieldB": valueC}]);
*/

db.users.find({$and: [{ "age": {$ne: 82} }, { "age": {$ne: 76} }] });

// Field Projection

/*
	- Retrieving documents are common operations that we do and by default by MongoDB queries return the whole document as a response
	- when dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish
	- to help with readability of the values returned, we can include/exclude fields from the response
*/

// Inclusion 

/*
	- allows us to include/add specific field only when retrieving documents
	- the value provided is 1 to denote that the field is being included

	Syntax:
		db.collectionName.find({criteria}, {"field": 1});
*/

db.users.find(
	{
		"firstName": "Jane"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact": 1
	}
);

// Exclusion

/*
	- allows us to exclude/remove specific field only when retrieving documents
	- the value provided is 0 to denote that the field is being excluded

	Syntax:
		db.collectionName.find({criteria}, {"field": 0});
*/

db.users.find(
	{
		"firstName": "Jane"
	},
	{
		"contact": 0,
		"department": 0
	}
);

// Supressing the ID field

db.users.find(
	{
		"firstName": "Jane"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact": 1,
		"_id": 0
	}
);

// Returning specific fields in embedded documents

db.users.find(
	{
		"firstName": "Jane"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact.phone": 1
	}
);

// Supressing specific fields in embedded documents

db.users.find(
	{
		"firstName": "Jane"
	},
	{
		"contact.phone": 0
	}
);

// Project specific array elements in returned array

// the $slice operator allows us to retrieve only 1 element that matches the search criteria

db.users.insertOne({
    namearr: [
        {
            "fname": "juan",
        },
        {
            "fname": "juan",
        }
    ]
});

db.users.find(
	{ "namearr": 
		{ 
			"fname": "juan" 
		} 
	}, 
	{ "namearr": 
		{ $slice: 1 } 
	}
);

// [Section] Evaluation Query Operators

// $regex

/*
	- allows us to find documents that match a specific string pattern using regular expressions

	Syntax:
		db.users.find({"field": $regex: "pattern", $options: "optionValue"})
*/

// Case sensitive query

db.users.find({"firstName": { $regex: "N"}});

db.users.find({"firstName": { $regex: "n"}});

db.users.find({"firstName": { $regex: "Steph"}});

//  .         .
/*Jane, Stephen*/

// Case insensitivity query

db.users.find({"firstName": { $regex: "j", $options: "i"}});