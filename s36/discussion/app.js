// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes deined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Connection
// app.js -> taskRoute.js -> taskController -> task.js

// Server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://lemuelcoros:YmoAeS5qkSZyrpRd@wdc028-course-booking.tdobmlh.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoute);

// http://localhost:4000/task/

// Server listening

if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;