// Function

/*
	
	Syntax:
		function functionName(){
	
				cod block (statement)
		};

*/

function printName(){
	console.log("My name is Lemuel");
};

printName();

// Function Declaration vs expressions

// Function Declaration
function declaredFunction(){
	console.log("Hello, world from declaredFunction()");
};

declaredFunction();

// Function Expression
let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();

declaredFunction = function(){
	console.log("Updated declaredFunction");
};

declaredFunction();

const constantFunc = function(){
	console.log("Initialized with const!");
};

constantFunc();

/*
constantFunc = function(){
	console.log("Cannot be rea-assigned");
};

constantFunc();
*/

// Function Scoping

/*
	Scope is the accessibility (visibility) of variables

	JS Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/

{
	let localVar = "Alonzo Mattheo";

	console.log(localVar);
}

let globalVar = "Aizaac Ellis";

console.log(globalVar);

function showName () {
	// Function Scope Variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

};

showName();

// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

// Nested Functions

function myNewFunction () {
	let name = "Jane";

	function nestedFunction () {
		let nestedName = "John";
		console.log(name);
	}


	nestedFunction();
}


myNewFunction();

// Function and Global Scoped Variables

// Global Scope Variable

let globalName = "Joy";

function myNewFunction2() {
	let nameInside = "Kenzo";

	console.log(globalName);
}

myNewFunction2();


// Using alert()

alert("Hello World"); // This will run immediately when the page loads

function showSampleAlert(){
	alert("Hello User!");
}

showSampleAlert();

// Using prompt()

let samplePrompt = prompt("Enter your name: ");

console.log("Hello, " + samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!")

}

printWelcomeMessage();

/*
	Function naming conventions
		- Function names shoud be definitive of the task it will perform
		- Avoid generic names to avoid confusion within the code
		- Avoid pointless and inappropriate function names
		- Name your functions follwing camel casing
		- Do not use JS reserved keywords

*/