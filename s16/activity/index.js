console.log("Hello World!");

// Activity Part 1

let num1 = 25;
let num2 = 5;
console.log("The result of num1 + num2 should be 30.");
console.log("Actual Result:");
console.log(num1 + num2);

let num3 = 156;
let num4 = 44;
console.log("The result of num3 + num4 should be 200.");
console.log("Actual Result:");
console.log(num3 + num4);

let num5 = 17;
let num6 = 10;
console.log("The result of num5 - num6 should be 7.");
console.log("Actual Result:");
console.log(num5-num6);

// Activity Part 2

let minutesHour = 60;
let hoursDay = 24;
let daysWeek = 7;
let weeksMonth = 4;
let monthsYear = 12;
let daysYear = 365;

let resultMinutes = (minutesHour / 1) * (hoursDay / 1) * (daysYear / 1);
console.log("There are " + resultMinutes + " minutes in a year.");

// Activity Part 3

let tempCelsius = 132;

let resultFahrenheit = (tempCelsius * (9/5)) + 32;
console.log("132 degrees Celsius when converted to Fahrenheit is " + resultFahrenheit);

// Activity Part 4a

let num7 = 165;
let moduloA = 165 % 8;
console.log("The remainder of 165 divided by 8 is: " + moduloA);
console.log("Is num7 divisible by 8?");

let isDivisibleBy8 = moduloA === 0;
console.log(isDivisibleBy8);


// Activity Part 4b

let num8 = 348;
let moduloB = 348 % 4
console.log("The remainder of 348 divided by 4 is: " + moduloB);
console.log("Is num8 divisible by 4?");

let isDivisibleBy4 = moduloB === 0;
console.log(isDivisibleBy4);
