// Activity 1

db.fruits.aggregate([
	{ $match: { "onSale": true} },
	{ $count: "fruitsOnSale"}
]);

// Activity 2

db.fruits.aggregate([
	{ $match: { "stock": { $gt: 20 } } },
	{ $count: "enoughStock"}
]);

// Activity 3

db.fruits.aggregate([
	{ $match: { "onSale": true} },
	{ $group: { _id: "$supplier_id", avg_price: { $avg: { $sum: "$price" } } } }
]);

// Activity 4

db.fruits.aggregate([
	{ $group: { _id: "$supplier_id", max_price: { $max: "$price" } } }
	
]);

// Activity 5

db.fruits.aggregate([
	{ $group: { _id: "$supplier_id", min_price: { $min: "$price" } } }
	
]);