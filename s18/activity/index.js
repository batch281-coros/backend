console.log("Hello World");

// Activity Part 1

function activitySum (num1, num2) {
	let sum = num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum);

};

activitySum(12, 5);

function activityDifference (num1, num2) {
	let difference = num1 - num2;
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(difference);

};

activityDifference(20, 3);

// Activity Part 2

function activityMultiply(num1, num2) {
	console.log("The product of " + num1 + " and " + num2 + ":"); 
	return num1 * num2;
}

let product = activityMultiply(4, 5);
console.log(product);

function activityDivision(num1, num2) {
	console.log("The quotient of " + num1 + " and " + num2 + ":"); 
	return num1 / num2;
}

let quotient = activityDivision(27, 3);
console.log(quotient);

// Activity Part 3

function circleArea(radius) {
	let pi = 3.1416;
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return pi * (radius ** 2);
}

let area = circleArea(15);
console.log(area);

// Activity Part 4

function average(num1, num2, num3, num4) {
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + " and " + num4 + ":");
	return (num1 + num2 + num3 + num4) / 4;
};

let averageResult = average(10, 30, 70, 10);
console.log(averageResult);

// Activity Part 5

function passCheck(num1, num2){
	let percentage = (num1 / num2) * 100;
	let isPassed = percentage >= 75;
	console.log("Is " + num1 + "/" + num2 + " a passing score?");
	return isPassed;
};

let isPassingScore = passCheck(55, 100);
console.log(isPassingScore);